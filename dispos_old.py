import pandas as pd
import os
import pyodbc
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime, date, time, timedelta
import calendar
import sys
import shutil
import numpy as np
try:
    import cPickle as pickle
except ImportError:  # python 3.x
    import pickle
import argparse
import locale
from dateutil.relativedelta import relativedelta
import operator
locale.setlocale(locale.LC_ALL, 'fr_BE')

# graphe présence sous-off 19-7 situ et sijo, et si assuré par off ou ss-off
# corriger titres (dates)

parser = argparse.ArgumentParser(description="Calcul de statistiques sur les disponibilités")
parser.add_argument('--day', '-d', action="store", type=int, dest='day')
parser.add_argument('--month', '-m', action="store", type=int, dest='month')
parser.add_argument('--year', '-y', action="store", type=int, dest='year')
args = parser.parse_args()

skip_plots = False
skip_hists = False
skip_means = False

# TODO
# CASHG -> SRP ?
# seuils 2, 4, 6 et 8
# générer histogrammes avec et sans SRP
# générer journées type par jour de la semaine
# get rotation équipes (garde/renfort)
# zonaux ?

# couleurs pour services dimanche et samedi+jour férié
sun_color = 'red'
sat_color = 'green'
week_color = 'blue'

thresholds = [2, 4, 6]

station_config = [
    #{'name' : 'SIBA', 'skip' : False, 'day_week' : (10, 6), 'night_week' : (8, 6), 'day_sat' : (10, 6), 'night_sat' : (8, 6), 'day_sun' : (8, 6), 'night_sun' : (8, 6), 'korpsid': 2},
    #{'name' : 'SIJO', 'skip' : False, 'day_week' : (4, 2), 'night_week' : (0, 0), 'day_sat' : (4, 2), 'night_sat' : (0, 0), 'day_sun' : (0, 0), 'night_sun' : (0, 0), 'korpsid': 4},
    #{'name' : 'SINI', 'skip' : False, 'day_week' : (8, 6), 'night_week' : (6, 6), 'day_sat' : (8, 6), 'night_sat' : (6, 6), 'day_sun' : (8, 6), 'night_sun' : (6, 6), 'korpsid': 3},
    #{'name' : 'SITU', 'skip' : False, 'day_week' : (6, 6), 'night_week' : (2, 6), 'day_sat' : (6, 6), 'night_sat' : (2, 6), 'day_sun' : (2, 6), 'night_sun' : (2, 6), 'korpsid': 5},
    {'name' : 'SIWA', 'skip' : False, 'day_week' : (10, 6), 'night_week' : (8, 6), 'day_sat' : (10, 6), 'night_sat' : (8, 6), 'day_sun' : (8, 6), 'night_sun' : (8, 6), 'korpsid': 1}
]

holidays = ['1/1/2016', '28/3/2016', '1/5/2016', '5/5/2016', '16/5/2016', '21/7/2016', '15/8/2016', '1/11/2016', '11/11/2016', '25/12/2016', 
'1/1/2017','17/4/2017','1/5/2017', '25/5/2017', '5/6/2017', '21/7/2017', '15/8/2017', '1/11/2017', '11/11/2017', '25/12/2017',
'1/1/2018', '1/4/2018', '2/4/2018', '1/5/2018', '10/5/2018', '20/5/2018', '21/5/2018', '21/7/2018', '15/8/2018', '1/11/2018', '11/11/2018', '25/12/2018',
'1/1/2019', '21/4/2019', '22/4/2019', '1/5/2019', '30/5/2019', '9/6/2019', '10/6/2019', '21/7/2019', '15/8/2019', '1/11/2019', '11/11/2019', '25/12/2019']

def is_holiday(search_date):
    return search_date in [datetime.strptime(x, "%d/%m/%Y").date() for x in holidays]

def printf(msg):
    with open('out.txt', 'a') as f:
        print(msg, file=f)

def connect_to_easycad_db(database):
    server = '10.113.64.13'
    username = 'EasyCADro'
    password = 'sJEG2eyd'
    cnxn = pyodbc.connect('DSN=EASYCAD-DB;DATABASE={};UID=EasyCADro;PWD=sJEG2eyd'.format(database))
    return cnxn

def get_all_from_db_as_dict(tablename, fields='*', where_clause='', database="EasyCAD"):
    conn = connect_to_easycad_db(database=database)
    sql = 'SELECT {} FROM dbo.{};'.format(fields, tablename)
    if where_clause != '':
        sql = sql[:-1] + ' WHERE {};'.format(where_clause)
    cursor = conn.cursor().execute(sql)
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results

def get_availabilities():
    return get_all_from_db_as_dict('StatPersonnelAvailability', fields='*', database="EasyCAD_stat")

def get_availabilities_for_shift(shift_start):
    return get_all_from_db_as_dict('StatPersonnelAvailability', fields='*', database="EasyCAD_stat", where_clause="datediff(day, DateTime, '{}') <= 1".format(shift_start.strftime('%Y-%m-%d')))

def get_team_subscriptions():
    return get_all_from_db_as_dict('TeamSubscriptions', fields='*')

def get_team_shifts():
    return get_all_from_db_as_dict('TeamShifts', fields='*')

def get_all_personnel():
    return get_all_from_db_as_dict('Personnel', fields='*')

def get_function_names():
    return get_all_from_db_as_dict('PersonnelFunctionNames')

def get_functions():
    return get_all_from_db_as_dict('PersonnelFunctions')

def get_team_groups():
    tgs = get_all_from_db_as_dict('TeamGroups')
    return [tg for tg in tgs if tg.get('KorpsID', 99) < 10]

def get_teams():
    return get_all_from_db_as_dict('Teams')

def is_officier(pers):
    return pers.get('GradeID') in [19, 20, 21, 26, 27, 29, 31]

def is_sous_officier(pers):
    return pers.get('GradeID') in [1, 3, 22, 24, 28, 30]

def is_sapeur(pers):
    return pers.get('GradeID') in [18, 23, 25]

def save_data_pickle(d, filename):
    with open(filename, 'wb') as fp:
        pickle.dump(d, fp, protocol=pickle.HIGHEST_PROTOCOL)

def load_data_pickle(filename):
    if not os.path.exists(filename):
        print("Le fichier {} n'existe pas".format(filename))
        return None
    with open(filename, 'rb') as fp:
        data = pickle.load(fp)
        return data

def make_day_bar_plot(df, start_shift, end_shift, title, out_dir, suffix):
    fig, ax = plt.subplots(figsize=(15,7))
    df[['effectif_principal', 'effectif_renfort', 'effectif_rappel']].plot(ax=ax, kind='area', stacked=True, grid=True, title=title, linewidth=0)
    ax.xaxis.set_major_locator(mdates.HourLocator())
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    plt.xlim(start_shift, end_shift)
    plt.ylim(0,16)
    plt.savefig(os.path.join(out_dir, "{}{}.jpg".format(start_shift.date(), suffix)))
    df['effectif_inter'].plot(ax=ax, kind='line', linewidth=1, legend=True)
    plt.savefig(os.path.join(out_dir, "{}{}-inter.jpg".format(start_shift.date(), suffix)))
    plt.close()

    fig, ax = plt.subplots(figsize=(15,7))
    df[['effectif_ssoff_principal', 'effectif_ssoff_renfort', 'effectif_ssoff_rappel']].plot(ax=ax, kind='area', stacked=True, grid=True, title=title, linewidth=0)
    ax.xaxis.set_major_locator(mdates.HourLocator())
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    plt.xlim(start_shift, end_shift)
    plt.ylim(0,16)
    plt.savefig(os.path.join(out_dir, "{}{}-ssoff.jpg".format(start_shift.date(), suffix)))
    plt.close()

def make_mean_plot(d, title, sg_hour, sg_minute, sg_error, out_dir, suffix, start_shift, end_shift, color):
    fig, ax = plt.subplots(figsize=(15,7))
    if not sg_minute is None:
        means_suffix = "minutes-"
        plt.plot(sg_minute.index, sg_minute.values, color='red')
    else:
        means_suffix = ''
    plt.bar(sg_hour.index, sg_hour.values, align='edge', edgecolor="black", linewidth=1, width=1.0, color=color)
    #plt.fill_between(sg_minute.index, sg_minute.values-sg_error.values, sg_minute.values+sg_error.values, alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')
    plt.title(title)
    plt.xlim(0, 24)
    plt.ylim(0, 12)
    hours = list()
    for i in range(0, 24):
        hours.append("{}h".format(i))
    plt.xticks(np.arange(0, 24), hours)
    plt.savefig(os.path.join(out_dir, "{}-jourtype-{}{}.jpg".format(d, means_suffix, suffix)))
    plt.close()

def make_stacked_mean_plot(d, title, sg_hour, sg_minute, sg_minute_error, srp_hour, srp_minutes, srp_minute_error, out_dir, suffix, start_shift, end_shift, color1, color2):
    fig, ax = plt.subplots(figsize=(15,7))
    if not sg_minute is None:
        means_suffix = "minutes-"
        plt.plot(sg_minute.index, srp_minutes.values, color='red')
        plt.plot(sg_minute.index, sg_minute.values, color='red')
    else:
        means_suffix = ''
    plt.bar(srp_hour.index, srp_hour.values, align='edge', edgecolor="black", linewidth=1, width=1.0, color=color1)
    plt.bar(sg_hour.index, sg_hour.values, align='edge', edgecolor="black", linewidth=1, width=1.0, bottom=srp_hour.values, color=color2)
    plt.title(title)
    plt.xlim(0, 24)
    #plt.ylim(0, max(sg_hour.values) + max(srp_hour.values) + 1)
    plt.ylim(0, 24)
    hours = list()
    for i in range(0, 24):
        hours.append("{}h".format(i))
    plt.xticks(np.arange(0, 24), hours)
    plt.savefig(os.path.join(out_dir, "{}-jourtype-{}{}.jpg".format(d, means_suffix, suffix)))
    plt.close()

def make_hist(df, title, hist_file):
    fig, ax = plt.subplots(figsize=(15,7))
    df.plot(ax=ax, kind='hist', grid=True, title=title, align='mid', range=(0,100), edgecolor='black', linewidth=1, xticks=range(0,101, 10))
    ax.set_ylabel("Nombre de jours")
    ax.set_xlabel("Pourcentage")
    plt.xlim(0,100)
    plt.savefig(hist_file)
    plt.close()

def tg_id_to_tg_name(list_of_tg, tgid):
    tg_name = next(tg for tg in list_of_tg if tg.get('TeamGroupID') == tgid).get('Name')
    prefix = '_GE_'
    prefix2 = '_GE-'
    if tg_name.startswith(prefix):
        return tg_name[len(prefix):]
    elif tg_name.startswith(prefix2):
        return tg_name[len(prefix2):]
    else:
        return tg_name

def get_teams_from_personnel_id(list_of_team_subscriptions, list_of_teams, list_of_team_groups, id):
    team_subscriptions = [t for t in list_of_team_subscriptions if t.get('PersonnelID') == id]
    team_names = [next(t for t in list_of_teams if t.get('TeamID') == ts.get('TeamID')) for ts in team_subscriptions]
    team_names = ['{} - {}'.format(t.get('TeamName'), tg_id_to_tg_name(list_of_team_groups, t.get('TeamGroupID'))) for t in team_names]
    return team_names

def time_to_datetime(ti):
    #return datetime.strptime(ti, "%H:%M:%S")
    return ti.hour + ti.minute/60.0
    return datetime.combine(date(2018,8,13), ti)

def hour_to_datetime(ti):
    return ti
    #return datetime.strptime(ti, "%H:%M:%S")
    return datetime.combine(date(2018,8,13), time(ti))

def gen_hists(hist_dir, start_date, end_date, s_name, percentages_under_thresholds, percentages_under_thresholds_week, percentages_under_thresholds_sat, percentages_under_thresholds_sun):

    perc_columns = ['Date']
    for thresh in thresholds:
        perc_columns.append('day-{}'.format(thresh))
        perc_columns.append('night-{}'.format(thresh))
        perc_columns.append('total-{}'.format(thresh))
        perc_columns.append('day-rap-{}'.format(thresh))
        perc_columns.append('night-rap-{}'.format(thresh))
        perc_columns.append('total-rap-{}'.format(thresh))

    #print(percentages_under_thresholds)

    percentages_under_thresholds_df = pd.DataFrame.from_records(percentages_under_thresholds, columns=perc_columns)
    percentages_under_thresholds_df_week = pd.DataFrame.from_records(percentages_under_thresholds_week, columns=perc_columns)
    percentages_under_thresholds_df_sat = pd.DataFrame.from_records(percentages_under_thresholds_sat, columns=perc_columns)
    percentages_under_thresholds_df_sun = pd.DataFrame.from_records(percentages_under_thresholds_sun, columns=perc_columns)

    for idx, thresh in enumerate(thresholds):

        max_threshold = max(thresholds)
        if thresh < max_threshold:
            suffix_title = " si {} pompier{} supplémentaire{} en SG".format(max_threshold-thresh, 's' if max_threshold-thresh!=1 else '', 's' if max_threshold-thresh!=1 else '')
        else:
            suffix_title = ''

        # Tous les jours
        make_hist(percentages_under_thresholds_df.iloc[:, 1 + idx*6],
            "{} du {} au {} : pourcentage des gardes avec effectif < {} {} - jour".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne', "{}-{}-{}-{}-jour.jpg".format(s_name, thresh, start_date, end_date)))
        make_hist(percentages_under_thresholds_df.iloc[:, 2 + idx*6],
            "{} du {} au {} : pourcentage des gardes avec effectif < {}{} - nuit".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne', "{}-{}-{}-{}-nuit.jpg".format(s_name, thresh, start_date, end_date)))
        make_hist(percentages_under_thresholds_df.iloc[:, 3 + idx*6],
            "{} du {} au {} : pourcentage des gardes avec effectif < {}{}".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne', "{}-{}-{}-{}.jpg".format(s_name, thresh, start_date, end_date)))

        make_hist(percentages_under_thresholds_df.iloc[:, 4 + idx*6],
            "{} du {} au {} : pourcentage des gardes avec effectif + rappel < {} {} - jour".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne_plus_rappel', "{}-{}-{}-{}-jour.jpg".format(s_name, thresh, start_date, end_date)))
        make_hist(percentages_under_thresholds_df.iloc[:, 5 + idx*6],
            "{} du {} au {} : pourcentage des gardes avec effectif + rappel < {}{} - nuit".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne_plus_rappel', "{}-{}-{}-{}-nuit.jpg".format(s_name, thresh, start_date, end_date)))
        make_hist(percentages_under_thresholds_df.iloc[:, 6 + idx*6],
            "{} du {} au {} : pourcentage des gardes avec effectif + rappel < {}{}".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne_plus_rappel', "{}-{}-{}-{}.jpg".format(s_name, thresh, start_date, end_date)))


        # Semaine
        make_hist(percentages_under_thresholds_df_week.iloc[:, 1 + idx*6],
            "{} du {} au {} : pourcentage des gardes \"semaine\" avec effectif < {}{} - jour".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne', "{}-{}-sem-{}-{}-jour.jpg".format(s_name, thresh, start_date, end_date)))
        make_hist(percentages_under_thresholds_df_week.iloc[:, 2 + idx*6],
            "{} du {} au {} : pourcentage des gardes \"semaine\" avec effectif < {}{} - nuit".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne', "{}-{}-sem-{}-{}-nuit.jpg".format(s_name, thresh, start_date, end_date)))
        make_hist(percentages_under_thresholds_df_week.iloc[:, 3 + idx*6],
            "{} du {} au {} : pourcentage des gardes \"semaine\" avec effectif < {}{}".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne', "{}-{}-sem-{}-{}.jpg".format(s_name, thresh, start_date, end_date)))

        make_hist(percentages_under_thresholds_df_week.iloc[:, 4 + idx*6],
            "{} du {} au {} : pourcentage des gardes \"semaine\" avec effectif + rappel < {}{} - jour".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne_plus_rappel', "{}-{}-sem-{}-{}-jour.jpg".format(s_name, thresh, start_date, end_date)))
        make_hist(percentages_under_thresholds_df_week.iloc[:, 5 + idx*6],
            "{} du {} au {} : pourcentage des gardes \"semaine\" avec effectif + rappel < {}{} - nuit".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne_plus_rappel', "{}-{}-sem-{}-{}-nuit.jpg".format(s_name, thresh, start_date, end_date)))
        make_hist(percentages_under_thresholds_df_week.iloc[:, 6 + idx*6],
            "{} du {} au {} : pourcentage des gardes \"semaine\" avec effectif + rappel < {}{}".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne_plus_rappel', "{}-{}-sem-{}-{}.jpg".format(s_name, thresh, start_date, end_date)))

        # Samedi / jour férié
        make_hist(percentages_under_thresholds_df_sat.iloc[:, 1 + idx*6],
            "{} du {} au {} : pourcentage des gardes \"samedi/jour férié\" avec effectif < {}{} - jour".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne', "{}-{}-samjf-{}-{}-jour.jpg".format(s_name, thresh, start_date, end_date)))
        make_hist(percentages_under_thresholds_df_sat.iloc[:, 2 + idx*6],
            "{} du {} au {} : pourcentage des gardes \"samedi/jour férié\" avec effectif < {}{} - nuit".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne', "{}-{}-samjf-{}-{}-nuit.jpg".format(s_name, thresh, start_date, end_date)))
        make_hist(percentages_under_thresholds_df_sat.iloc[:, 3 + idx*6],
            "{} du {} au {} : pourcentage des gardes \"samedi/jour férié\" avec effectif < {}{}".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne', "{}-{}-samjf-{}-{}.jpg".format(s_name, thresh, start_date, end_date)))


        make_hist(percentages_under_thresholds_df_sat.iloc[:, 4 + idx*6],
            "{} du {} au {} : pourcentage des gardes \"samedi/jour férié\" avec effectif + rappel < {}{} - jour".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne_plus_rappel', "{}-{}-samjf-{}-{}-jour.jpg".format(s_name, thresh, start_date, end_date)))
        make_hist(percentages_under_thresholds_df_sat.iloc[:, 5 + idx*6],
            "{} du {} au {} : pourcentage des gardes \"samedi/jour férié\" avec effectif + rappel < {}{} - nuit".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne_plus_rappel', "{}-{}-samjf-{}-{}-nuit.jpg".format(s_name, thresh, start_date, end_date)))
        make_hist(percentages_under_thresholds_df_sat.iloc[:, 6 + idx*6],
            "{} du {} au {} : pourcentage des gardes \"samedi/jour férié\" avec effectif + rappel < {}{}".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne_plus_rappel', "{}-{}-samjf-{}-{}.jpg".format(s_name, thresh, start_date, end_date)))

        # Dimanche
        make_hist(percentages_under_thresholds_df_sun.iloc[:, 1 + idx*6],
            "{} du {} au {} : pourcentage des gardes \"dimanche\" avec effectif < {}{} - jour".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne', "{}-{}-dim-{}-{}-jour.jpg".format(s_name, thresh, start_date, end_date)))
        make_hist(percentages_under_thresholds_df_sun.iloc[:, 2 + idx*6],
            "{} du {} au {} : pourcentage des gardes \"dimanche\" avec effectif < {}{} - nuit".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne', "{}-{}-dim-{}-{}-nuit.jpg".format(s_name, thresh, start_date, end_date)))
        make_hist(percentages_under_thresholds_df_sun.iloc[:, 3 + idx*6],
            "{} du {} au {} : pourcentage des gardes \"dimanche\" avec effectif < {}{}".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne', "{}-{}-dim-{}-{}.jpg".format(s_name, thresh, start_date, end_date)))


        make_hist(percentages_under_thresholds_df_sun.iloc[:, 4 + idx*6],
            "{} du {} au {} : pourcentage des gardes \"dimanche\" avec effectif + rappel < {}{} - jour".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne_plus_rappel', "{}-{}-dim-{}-{}-jour.jpg".format(s_name, thresh, start_date, end_date)))
        make_hist(percentages_under_thresholds_df_sun.iloc[:, 5 + idx*6],
            "{} du {} au {} : pourcentage des gardes \"dimanche\" avec effectif + rappel < {}{} - nuit".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne_plus_rappel', "{}-{}-dim-{}-{}-nuit.jpg".format(s_name, thresh, start_date, end_date)))
        make_hist(percentages_under_thresholds_df_sun.iloc[:, 6 + idx*6],
            "{} du {} au {} : pourcentage des gardes \"dimanche\" avec effectif + rappel < {}{}".format(s_name, start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), thresh, suffix_title),
            os.path.join(hist_dir, 'caserne_plus_rappel', "{}-{}-dim-{}-{}.jpg".format(s_name, thresh, start_date, end_date)))

def gen_means(s_name, pers_count_df, start_date, end_date, out_dir):

    for d in range(0,7):

        days_fr = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche']
        
        pers_count_df['time'] = pers_count_df.index.time
        pers_count_df['hour'] = pers_count_df.index.hour
        pers_count_df['datetime'] = pers_count_df.index
        pers_count_day = pers_count_df.loc[pers_count_df['weekday'] == d]

        mins_groups_day = pers_count_day.groupby('time')

        means_mins = mins_groups_day.mean()
        errors_mins = mins_groups_day.std()

        means_mins['datetime'] = means_mins.index.map(time_to_datetime)
        errors_mins['datetime'] = errors_mins.index.map(time_to_datetime)

        means_mins.index = means_mins['datetime']
        errors_mins.index = errors_mins['datetime']

        means_hours = means_mins.groupby('hour').mean()

        make_mean_plot(d+1, "{} - SRP - jour type - {} ({}-{})".format(s_name.upper(), days_fr[d], start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y")), means_hours['effectif_rappel'], means_mins['effectif_rappel'], errors_mins['effectif_rappel'], os.path.join(out_dir, 'rappel', 'jour'), days_fr[d], datetime.combine(date(2018,8,13), time(0)), datetime.combine(date(2018,8,13), time(23, 59)), color='green')
        make_mean_plot(d+1, "{} - SRP - jour type - {} ({}-{})".format(s_name.upper(), days_fr[d], start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y")), means_hours['effectif_rappel'], None, None, os.path.join(out_dir, 'rappel', 'jour'), days_fr[d], datetime.combine(date(2018,8,13), time(0)), datetime.combine(date(2018,8,13), time(23, 59)), color='green')

        make_mean_plot(d+1, "{} - SG - jour type - {} ({}-{})".format(s_name.upper(), days_fr[d], start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y")), means_hours['effectif_total'],  means_mins['effectif_total'], errors_mins['effectif_total'], os.path.join(out_dir, 'caserne', 'jour'), days_fr[d], datetime.combine(date(2018,8,13), time(0)), datetime.combine(date(2018,8,13), time(23, 59)), color='blue')
        make_mean_plot(d+1, "{} - SG - jour type - {} ({}-{})".format(s_name.upper(), days_fr[d], start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y")), means_hours['effectif_total'], None, None, os.path.join(out_dir, 'caserne', 'jour'), days_fr[d], datetime.combine(date(2018,8,13), time(0)), datetime.combine(date(2018,8,13), time(23, 59)), color='blue')

        make_stacked_mean_plot(d+1, "{} - SRP + SG - jour type - {} ({}-{})".format(s_name.upper(), days_fr[d], start_date.strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y")), means_hours['effectif_total'], None, None, means_hours['effectif_rappel'], None, None, os.path.join(out_dir, 'srp_plus_sg', 'jour'), days_fr[d], datetime.combine(date(2018,8,13), time(0)), datetime.combine(date(2018,8,13), time(23, 59)), color1='green', color2='blue')

    iter_date = start_date.replace(day=1)
    while iter_date <= end_date:

        m = iter_date.month
        y = iter_date.year

        month_fr = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'décembre']

        pers_count_df['time'] = pers_count_df.index.time
        pers_count_df['hour'] = pers_count_df.index.hour
        pers_count_df['datetime'] = pers_count_df.index
        pers_count_df['month'] = pers_count_df.index.month
        pers_count_df['year'] = pers_count_df.index.year
        pers_count_month = pers_count_df.loc[pers_count_df['month'] == m]

        mins_groups_month = pers_count_month.groupby('time')
        means_mins = mins_groups_month.mean()
        errors_mins = mins_groups_month.std()

        means_mins['datetime'] = means_mins.index.map(time_to_datetime)
        errors_mins['datetime'] = errors_mins.index.map(time_to_datetime)

        means_mins.index = means_mins['datetime']
        errors_mins.index = errors_mins['datetime']

        means_hours = means_mins.groupby('hour').mean()

        make_mean_plot(m, "{} - SRP - jour type - {} {}".format(s_name.upper(), month_fr[m-1], y), means_hours['effectif_rappel'], means_mins['effectif_rappel'], errors_mins['effectif_rappel'], os.path.join(out_dir, 'rappel', 'mois'), month_fr[m-1], datetime.combine(date(2018,8,13), time(0)), datetime.combine(date(2018,8,13), time(23, 59)), color='green')
        make_mean_plot(m, "{} - SRP - jour type - {} {}".format(s_name.upper(), month_fr[m-1], y), means_hours['effectif_rappel'], None, None, os.path.join(out_dir, 'rappel', 'mois'), month_fr[m-1], datetime.combine(date(2018,8,13), time(0)), datetime.combine(date(2018,8,13), time(23, 59)), color='green')

        make_mean_plot(m, "{} - SG - jour type - {} {}".format(s_name.upper(), month_fr[m-1], y), means_hours['effectif_total'],  means_mins['effectif_total'], errors_mins['effectif_total'], os.path.join(out_dir, 'caserne', 'mois'), month_fr[m-1], datetime.combine(date(2018,8,13), time(0)), datetime.combine(date(2018,8,13), time(23, 59)), color='blue')
        make_mean_plot(m, "{} - SG - jour type - {} {}".format(s_name.upper(), month_fr[m-1], y), means_hours['effectif_total'], None, None, os.path.join(out_dir, 'caserne', 'mois'), month_fr[m-1], datetime.combine(date(2018,8,13), time(0)), datetime.combine(date(2018,8,13), time(23, 59)), color='blue')

        make_stacked_mean_plot(m+1, "{} - SRP + SG - jour type - {} {}".format(s_name.upper(), month_fr[m-1], y), means_hours['effectif_total'], None, None, means_hours['effectif_rappel'], None, None, os.path.join(out_dir, 'srp_plus_sg', 'mois'), month_fr[m-1], datetime.combine(date(2018,8,13), time(0)), datetime.combine(date(2018,8,13), time(23, 59)), color1='green', color2='blue')

        iter_date += relativedelta(months=1)

def last_day_of_month(any_day):
    next_month = any_day.replace(day=28) + timedelta(days=4)
    return next_month - timedelta(days=next_month.day)

def daily_analysis(year, month, day):

    analysis_date = date(year, month, day)

    # dossiers de destination
    year_dir = os.path.join('out', str(year))
    month_dir = os.path.join('out', str(year), str(month))
    out_dir = os.path.join('out', str(year), str(month))
    data_dir = os.path.join('out', str(year), str(month), 'data_input')
    calc_dir = os.path.join('out', str(year), str(month), 'data_calc')
    graph_dir = os.path.join('out', str(year), str(month), 'jours')

    # création de l'arborescence si besoin
    if not os.path.exists(year_dir):
        os.makedirs(year_dir)
    if not os.path.exists(month_dir):
        os.makedirs(month_dir)
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    if not os.path.exists(graph_dir):
        os.makedirs(graph_dir)
    if not os.path.exists(calc_dir):
        os.makedirs(calc_dir)

    for s in station_config:
        if not os.path.exists(os.path.join(graph_dir, s['name'].lower())):
            os.makedirs(os.path.join(graph_dir, s['name'].lower()))
        if not os.path.exists(os.path.join(calc_dir, s['name'].lower())):
            os.makedirs(os.path.join(calc_dir, s['name'].lower()))

    # récupération des données et stockage
    base_data_file = os.path.join(data_dir, '{}.p'.format(analysis_date.strftime("%Y-%m-%d")))

    if os.path.exists(base_data_file):
        print("Données déjà téléchargées")
        everything = load_data_pickle(base_data_file)
        availabilities = everything['availabilities']
        team_subscriptions = everything['team_subscriptions']
        personnel = everything['personnel']
        team_shifts = everything['team_shifts']
        functions = everything['functions']
        team_groups = everything['team_groups']
        teams = everything['teams']
    else:        
        availabilities = get_availabilities_for_shift(analysis_date)
        team_subscriptions = get_team_subscriptions()
        personnel = get_all_personnel()
        team_shifts = get_team_shifts()
        functions = get_functions()
        team_groups = get_team_groups()
        teams = get_teams()
        everything = {'availabilities':availabilities, 'team_subscriptions': team_subscriptions, 'personnel':personnel, 'team_shifts':team_shifts, 'functions':functions, 'team_groups':team_groups, 'teams':teams}
        save_data_pickle(everything, base_data_file)

    # sort by date
    availabilities = sorted(availabilities, key=lambda k: k['DateTime'])

    # process
    for s in station_config:

        if s['skip']:
            continue

        # Init
        acc_perc_avail = 0
        korpsid = s['korpsid']
        s_name = s['name']
        presence_list_per_min = dict()
        presence_list_per_min_reinforcement = dict()
        presence_list_per_min_callable = dict()
        presence_list_per_min_inter = dict()
        presence_list_per_min_soff = dict()
        presence_list_per_min_reinforcement_soff = dict()
        presence_list_per_min_callable_soff = dict()
        presence_list_per_min_inter_soff = dict()
        presence_list_per_min_off = dict()
        presence_list_per_min_off_duty = dict()
        presence_list_per_min_off_callable = dict()
        presence_list_per_min_off_non_duty = dict()
        presence_list_per_min_off_inter = dict()

        pers_count_df = pd.DataFrame()

        # get all changes linked to that day
        changes = [a for a in availabilities if a.get('DateTime') >= datetime.combine(analysis_date, time(6, 55)) and 
            a.get('DateTime') <= (datetime.combine(analysis_date, time(6,50)) + timedelta(days=1)) and a.get('CurrentKorpsID') == korpsid]

        # test if data exists
        if len(changes) == 0:
            print("Pas de donnée pour le poste de {} le {}".format(s_name, analysis_date))
            continue

        # reset presence status
        pers_is_present = dict()
        pers_is_callable = dict()
        pers_is_inter = dict()
        off_is_duty = dict()
        off_is_non_duty = dict()

        live_count = 0
        live_count_reinforcement = 0
        live_count_callable = 0
        live_count_inter = 0

        live_count_soff = 0
        live_count_reinforcement_soff = 0
        live_count_callable_soff = 0
        live_count_inter_soff = 0
        
        live_count_off = 0
        live_count_off_duty = 0
        live_count_off_non_duty = 0
        live_count_off_callable = 0
        live_count_off_inter = 0

        pers_count = dict()

        start_shift_day = datetime.combine(analysis_date, time(7))
        end_shift_day = datetime.combine(analysis_date, time(19))
        start_shift_night = datetime.combine(analysis_date, time(19))
        end_shift_night = datetime.combine(analysis_date, time(7)) + timedelta(days=1)

        start_shift = datetime.combine(analysis_date, time(7))
        end_shift = datetime.combine(analysis_date, time(7)) + timedelta(days=1)

        # process them
        for line in changes:

            # get details
            pid = line.get('PersonnelID')
            freelevel = line.get('FreeLevel')
            dt = line.get('DateTime')
            rounded_dt = dt.replace(second=0, microsecond=0)
            pid = line.get('PersonnelID')

            # 1 Garde caserne
            # 2 En Intervention
            # 3 Ambulance 1er départ
            # 4 Ambulance 2ème départ Divers
            # 5 Caserne hors garde
            # 6 SRP
            # 7 SRNP 10'
            # 8 SRNP 20'
            # 9 Sous - Officier SRNP
            # 10 Vers poste
            # 11 Sous-officier SRP
            # 12 Indisponible
            # 13 Officier de garde
            # 14 Officier caserne hors garde
            # 15 Sous - Officier garde caserne
            # 16 Chauffeur autopompe (pompe)
            # 17 Garde caserne divers
            # 18 Dispo Spécialité
            # 19 Ambulance 3ème départ Divers
            # 20 Officie SRNP

            # track status
            is_now_inter = freelevel == 2
            is_now_dispo = freelevel in [1, 3, 4, 5, 15, 16, 17, 19]
            is_now_callable = freelevel in [6, 7, 8, 9, 10, 20]
            is_now_off_duty = freelevel == 13
            is_now_off_non_duty = freelevel == 14
            is_now_off_callable = freelevel == 20
            was_dispo = pers_is_present.get(pid, False)
            was_callable = pers_is_callable.get(pid, False)
            was_inter = pers_is_inter.get(pid, False)
            was_off_duty = off_is_duty.get(pid, False)
            was_off_non_duty = off_is_non_duty.get(pid, False)

            # get personnel details
            try:
                pers = next(p for p in personnel if p.get('PersonnelID') == pid)
                pers_functions = [(f.get('KorpsID'), f.get('FunctionID')) for f in functions if f.get('PersonnelID') == pers.get('PersonnelID')]
                pers_teams = get_teams_from_personnel_id(team_subscriptions, teams, team_groups, pers.get('PersonnelID'))
            except StopIteration:
                pers_functions = []
                pers_teams = []



            # test if officer, ss-off or sap
            is_ss_off = False
            is_off = False
            if is_officier(pers):
                is_off = True
            if is_sous_officier(pers):
                is_ss_off = True                    

            # test if it's a vol
            is_vol = False
            for t in pers_teams:
                if 'volontaire' in t.lower():
                    is_vol = True
                    break

            # record status
            pers_is_present[pid] = is_now_dispo
            off_is_present[pid] = is_now_off_duty or is_now_off_non_duty
            pers_is_callable[pid] = is_now_callable or is_now_off_callable
            pers_is_inter[pid] = is_now_inter
            if pid == 111:
                print(line)

            # +1 / -1
            if is_now_dispo:
                if not was_dispo:
                    if is_vol:
                        if is_ss_off:
                            live_count_reinforcement_soff += 1
                        live_count_reinforcement += 1
                    else:
                        if is_ss_off:
                            live_count_soff += 1
                        live_count += 1
                        #####
                        pers_count[pid] = pers_count.get(pid, 0) + 1
                        if pid == 111:
                            print("+++++")
                            print("")
                        #####
            elif was_dispo:
                    if is_vol:
                        if is_ss_off:
                            live_count_reinforcement_soff = live_count_reinforcement_soff - 1
                        live_count_reinforcement = live_count_reinforcement - 1
                    else:
                        if is_ss_off:
                            live_count_soff = live_count_soff - 1
                        live_count = live_count - 1
                        #####
                        pers_count[pid] = pers_count.get(pid, 0) - 1
                        if pid == 111:
                            print("-----")
                            print("")
                        ####


            if is_now_callable:
                if not was_callable:
                    if is_ss_off:
                            live_count_callable_soff += 1
                    live_count_callable += 1
            elif was_callable:
                    if is_ss_off:
                           live_count_callable_soff = live_count_callable_soff - 1
                    live_count_callable = live_count_callable - 1

            if is_now_off_duty:
                if not was_dispo:
                    live_count_off_duty += 1
            elif is_now_off_non_duty:
                if not was_dispo:
                    live_count_off_non_duty += 1
            elif was_off_dispo:

            if is_now_off_callable:
                if not was_callable:
                    live_count_off_callable += 1                

            if is_now_inter:
                if not was_inter:
                    if is_off:
                        live_count_off_inter += 1
                    else:
                        if is_ss_off:
                            live_count_inter_soff += 1
                        live_count_inter += 1
            else:
                if was_inter:
                    if is_off:
                        live_count_off_inter = live_count_off_inter - 1
                    else:
                        if is_ss_off:
                           live_count_inter_soff = live_count_inter_soff - 1
                        live_count_inter = live_count_inter - 1




            presence_list_per_min[rounded_dt] = live_count
            presence_list_per_min_reinforcement[rounded_dt] = live_count_reinforcement
            presence_list_per_min_callable[rounded_dt] = live_count_callable
            presence_list_per_min_inter[rounded_dt] = live_count_inter

            presence_list_per_min_soff[rounded_dt] = live_count_soff
            presence_list_per_min_reinforcement_soff[rounded_dt] = live_count_reinforcement_soff
            presence_list_per_min_callable_soff[rounded_dt] = live_count_callable_soff
            presence_list_per_min_inter_soff[rounded_dt] = live_count_inter_soff

            presence_list_per_min_off[rounded_dt] = live_count_off_duty + live_count_off_non_duty
            presence_list_per_min_off_duty[rounded_dt] = live_count_off_duty
            presence_list_per_min_off_callable[rounded_dt] = live_count_off_callable
            presence_list_per_min_off_non_duty[rounded_dt] = live_count_off_non_duty
            presence_list_per_min_off_inter[rounded_dt] = live_count_off_inter
       

        print(pers_count)

        
        # thresholds en fonction du type de jour
        # dimanche
        if analysis_date.weekday() == 6:
            color = sun_color
            suffix = '-D'
        elif analysis_date.weekday() == 5 or is_holiday(analysis_date):
            color = sat_color
            suffix = '-SJF'
        else:
            color = week_color
            suffix=''

        # generate dataframe for day
        df = pd.DataFrame.from_dict(presence_list_per_min, orient='index', columns=['effectif_principal'])
        df_reinf = pd.DataFrame.from_dict(presence_list_per_min_reinforcement, orient='index', columns=['effectif_renfort'])
        df_callable = pd.DataFrame.from_dict(presence_list_per_min_callable, orient='index', columns=['effectif_rappel'])
        df_inter = pd.DataFrame.from_dict(presence_list_per_min_inter, orient='index', columns=['effectif_inter'])
        df_soff = pd.DataFrame.from_dict(presence_list_per_min_soff, orient='index', columns=['effectif_ssoff_principal'])
        df_reinf_soff = pd.DataFrame.from_dict(presence_list_per_min_reinforcement_soff, orient='index', columns=['effectif_ssoff_renfort'])
        df_callable_soff = pd.DataFrame.from_dict(presence_list_per_min_callable_soff, orient='index', columns=['effectif_ssoff_rappel'])
        df_inter_soff = pd.DataFrame.from_dict(presence_list_per_min_inter_soff, orient='index', columns=['effectif_ssoff_inter'])
        df = df.join(df_reinf)
        df = df.join(df_callable)
        df = df.join(df_inter)
        df = df.join(df_soff)
        df = df.join(df_reinf_soff)
        df = df.join(df_callable_soff)
        df = df.join(df_inter_soff)
        df['effectif_total'] = df['effectif_principal'] + df['effectif_renfort']
        df['effectif_ssoff_total'] = df['effectif_ssoff_principal'] + df['effectif_ssoff_renfort']
        df['effectif_total_rappel'] = df['effectif_total'] + df['effectif_rappel']
        df['effectif_ssoff_total_rappel'] = df['effectif_ssoff_total'] + df['effectif_ssoff_rappel']

        df = df.reindex(pd.date_range(start=start_shift, end=end_shift, freq='1min'), method='ffill')
        df_day = df.reindex(pd.date_range(start=start_shift_day, end=end_shift_day, freq='1min'), method='ffill')
        df_night = df.reindex(pd.date_range(start=start_shift_night, end=end_shift_night, freq='1min'), method='ffill')
        df.index.name='heure'
        df_day.index.name='heure'
        df_night.index.name='heure'
        df['weekday'] = df.index.weekday
        
        title = "{} - {}".format(s_name, analysis_date)
        make_day_bar_plot(df, start_shift, end_shift, title, os.path.join(graph_dir, s_name.lower()), suffix)

        tot_min_day = df_day.effectif_total.count()
        tot_min_night = df_night.effectif_total.count()

        percs = [analysis_date]

        for thresh in thresholds:

            # effectif caserne
            day_perc = df_day[df_day <= thresh].effectif_total.count()/tot_min_day * 100
            night_perc = df_night[df_night <= thresh].effectif_total.count()/tot_min_night * 100
            percs.append(day_perc)
            percs.append(night_perc)
            percs.append((day_perc + night_perc) / 2)

            # effectif caserne + rappel
            day_perc_rap = df_day[df_day <= thresh].effectif_total_rappel.count()/tot_min_day * 100
            night_perc_rap = df_night[df_night <= thresh].effectif_total_rappel.count()/tot_min_night * 100
            percs.append(day_perc_rap)
            percs.append(night_perc_rap)
            percs.append((day_perc_rap + night_perc_rap) / 2)

        # sauvegarde des données
        data_to_save = {'presences':df, 'percs':percs}
        save_data_pickle(data_to_save, os.path.join(calc_dir, s_name.lower(), '{}.p'.format(analysis_date.strftime('%Y-%m-%d'), s_name.lower())))

def monthly_analysis(year, month):

    start_date = date(year,month,1)
    end_date = last_day_of_month(start_date)

    # dossiers de destination
    year_dir = os.path.join('out', str(year))
    month_dir = os.path.join('out', str(year), str(month))
    out_dir = os.path.join('out', str(year), str(month))
    data_dir = os.path.join('out', str(year), str(month), 'data_input')
    calc_dir = os.path.join('out', str(year), str(month), 'data_calc')
    graph_dir = os.path.join('out', str(year), str(month), 'jours')
    hist_dir = os.path.join('out', str(year), str(month), 'hist')
    means_dir = os.path.join('out', str(year), str(month), 'moyennes')

    # nettoyage des dossiers hist et moyennes
    shutil.rmtree(hist_dir, ignore_errors=True)
    shutil.rmtree(means_dir, ignore_errors=True)
    for s in station_config:
        os.makedirs(os.path.join(hist_dir, s['name'].lower()))
        os.makedirs(os.path.join(means_dir, s['name'].lower()))
        os.makedirs(os.path.join(hist_dir, s['name'].lower(), 'caserne'))
        os.makedirs(os.path.join(hist_dir, s['name'].lower(), 'caserne_plus_rappel'))
        os.makedirs(os.path.join(means_dir, s['name'].lower(), 'rappel'))
        os.makedirs(os.path.join(means_dir, s['name'].lower(), 'rappel', 'jour'))
        os.makedirs(os.path.join(means_dir, s['name'].lower(), 'rappel', 'mois'))
        os.makedirs(os.path.join(means_dir, s['name'].lower(), 'caserne'))
        os.makedirs(os.path.join(means_dir, s['name'].lower(), 'caserne', 'jour'))
        os.makedirs(os.path.join(means_dir, s['name'].lower(), 'caserne', 'mois'))
        os.makedirs(os.path.join(means_dir, s['name'].lower(), 'srp_plus_sg'))
        os.makedirs(os.path.join(means_dir, s['name'].lower(), 'srp_plus_sg', 'jour'))
        os.makedirs(os.path.join(means_dir, s['name'].lower(), 'srp_plus_sg', 'mois'))
    
    # calcul
    for s in station_config:

        if s['skip']:
            continue

        # Init
        d = start_date
        korpsid = s['korpsid']
        s_name = s['name']
        s_calc_dir = os.path.join(calc_dir, s['name'].lower())
        num_days = 0

        percentages_under_thresholds = []
        percentages_under_thresholds_week = []
        percentages_under_thresholds_sat = []
        percentages_under_thresholds_sun = []
        pers_count_df = pd.DataFrame()

        # Iterate over all days (7 -> 7)
        while d <= end_date:

            # get data
            d_str = d.strftime("%Y-%m-%d")
            data = load_data_pickle(os.path.join(s_calc_dir, "{}.p".format(d_str)))
            df = data['presences']
            percs = data['percs']

            # construction inctrémentale des données du mois
            pers_count_df = pers_count_df.append(df)
            percentages_under_thresholds.append(percs)

            if d.weekday() == 6:
                percentages_under_thresholds_sun.append(percs)
            elif d.weekday() == 5 or is_holiday(d):
                percentages_under_thresholds_sat.append(percs)
            else:
                percentages_under_thresholds_week.append(percs)

            d += timedelta(days=1)
            num_days += 1

        # Save data to file for later use
        save_data_pickle(pers_count_df, os.path.join(calc_dir, s_name.lower(), '{}-{}.p'.format(d.year, d.month)))
        save_data_pickle((percentages_under_thresholds, percentages_under_thresholds_week, percentages_under_thresholds_sat, percentages_under_thresholds_sun), os.path.join(calc_dir, s_name.lower(), '{}-{}-percs.p'.format(d.year, d.month)))

        # generate histograms
        gen_hists(os.path.join(hist_dir, s_name.lower()), start_date, end_date, s_name, percentages_under_thresholds, percentages_under_thresholds_week, percentages_under_thresholds_sat, percentages_under_thresholds_sun)

        # Generate mean days
        gen_means(s_name, pers_count_df, start_date, end_date, os.path.join(means_dir, s_name.lower()))

if __name__ == '__main__':

    if args.year:
        year = args.year

        if args.month:
            month = args.month
            
            if args.day:
                day = args.day
                print("Analyse journalière pour le {}/{}/{}".format(day, month, year))
                daily_analysis(year, month, day)
            else:
                print("Analyse mensuelle pour le mois {}/{}".format(month, year))
                monthly_analysis(year, month)
        else:
            print("Analyse annuelle - à faire")
    else:
        today = datetime.date.today()
        daily_analysis(today.year, today.month, today.day)

        if today.day == 1:

            last_month = today - datetime.timedelta(days=5)
            monthly_analysis(last_month.year, last_month.month)



